var http = require('http');
var util = require('util');
var express = require('express');
var path = require('path');
var ejs = require('ejs');
var fs = require('fs');
var app = express();
var c = require('child_process');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var jsdom = require('jsdom');
var li = 0;//当前下载启始值
var list = require('./build/json/list.json');

//设置静态文件夹
app.use(express.static(path.join(__dirname, '')));
//设置模板文件夹
app.set('views', path.join(__dirname, ''));
app.engine('.html', ejs.__express);
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(function (req, res, next) {
    next();
});
app.get('/app/getList', function (req, res) {
    apiList();
});
app.get('/api/getList', function (req, res) {
    res.send(list);
});
app.get('/app/getImage', function (req, res) {
    saveImage();
});

app.listen(2017, function () {
    // c.exec('start http://localhost:2017');
});


//得体所有值
function apiList() {
    var path = 'http://wiki.52poke.com/wiki/%E5%AE%9D%E5%8F%AF%E6%A2%A6%E5%88%97%E8%A1%A8%EF%BC%88%E6%8C%89%E5%85%A8%E5%9B%BD%E5%9B%BE%E9%89%B4%E7%BC%96%E5%8F%B7%EF%BC%89/%E7%AE%80%E5%8D%95%E7%89%88';
    jsdom.env(
        path,
        ["http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"],
        function (errors, window) {
            var eplist = window.$('.a-c.roundy.eplist');
            var data = {
                rows: []
            };
            try {
                for (var i in eplist.find('tr')) {
                    var tr = eplist.find('tr').eq(i);
                    var o = {
                        id: tr.find('td').eq(0).html(),
                        name: tr.find('td').eq(1).find('a').html(),
                        href: tr.find('td').eq(1).find('a').attr('href'),
                        ja: tr.find('td').eq(2).find('a').html(),
                        en: tr.find('td').eq(3).find('a').html(),
                    };
                    // o.id = o.id.replace('\n', '');
                    // getImage(o);
                    if (o.name)
                        data.rows.push(o);
                }

            } catch (e) {
                console.log(e);
            }
            saveJson('list.json', JSON.stringify(data));
            // rs.send(data);
        }
    );
};

//得到并下载所有图片
function getImage() {
    var o = list.rows[li];
    jsdom.env(
        'http://wiki.52poke.com' + o.href,
        ["http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"], function (errors, window) {
            var src = window.$('.roundy.bgwhite.fulltable a.image img').eq(0).attr('data-url');
            saveImage(src, li + "_" + o.en);
            li++;
            if (li < list.rows.length) {
                getImage();
            }
        });
}

//判断时候有指定文件
function fsExistsSync(path) {
    try {
        fs.accessSync(path, fs.F_OK);
    } catch (e) {
        return false;
    }
    return true;
}

//保存文件
function saveImage(url, en) {
    var srcFs = __dirname + '/build/images/upload/' + en + ".png"
    if (!fsExistsSync(srcFs)) {
        http.get("http:" + url, function (res) {
            var imgData = "";
            res.setEncoding("binary"); //一定要设置response的编码为binary否则会下载下来的图片打不开
            res.on("data", function (chunk) {
                imgData += chunk;
            });
            res.on("end", function () {
                fs.writeFile(srcFs, imgData, "binary", function (err) {
                    if (err) {
                        console.error(err);
                    }
                });
            });
        });
    }
    return srcFs.replace(__dirname, '');
}

//保存Json
function saveJson(name, data) {
    fs.writeFile(__dirname + '/build/json/' + name, data, "utf-8", function (err) {
        if (err) {
            console.error(err);
        }
    });
}



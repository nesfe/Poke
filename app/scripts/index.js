import './lib/vendor.js';
import 'bootstrap';
import Vue from 'vue';
import {router} from './lib/router.js';

new Vue({
    router: router
}).$mount('#main');

let touchmove = () => {
    if ($(window).scrollTop() > 50) {
        $('#navbar').addClass('active');
    } else {
        $('#navbar').removeClass('active');
    }
};
window.addEventListener('touchmove', touchmove);
$(window).scroll(touchmove);
const path = require('path');
const webpack = require('webpack');
const uglifyJsPlugin = webpack.optimize.UglifyJsPlugin;


const vendor = [
    'jquery',
    'vue',
    'vue-router',
    'vue-resource',
    'bootstrap'
];

module.exports = {
    entry: {
        vendor: vendor
    },
    output: {
        path: path.resolve(__dirname, 'build/vendor'),
        filename: "[name].js",
        library: "[name]_[hash]"
    },
    plugins: [
        new webpack.DllPlugin({
            path: path.resolve(__dirname, 'build/vendor', '[name]-manifest.json'),
            name: "[name]_[hash]",
            context: __dirname
        }),
        new uglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
};
const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const config = require('./config.json');
const srcDir = path.resolve(process.cwd(), config.app);

const getEntry = function () {
    let jsPath = path.resolve(srcDir, 'scripts');
    let dirs = fs.readdirSync(jsPath);
    let matchs = [], files = {};
    dirs.forEach(function (item) {
        matchs = item.match(/(.+)\.js$/);
        if (matchs) {
            files[matchs[1]] = path.resolve(srcDir, 'scripts', item);
        }
    });
    return files;
};
module.exports = {
    cache: true,
    devtool: "source-map",
    entry: getEntry(),
    output: {
        path: path.join(__dirname, config.build + "/scripts/"),
        publicPath: config.build + "/scripts/",
        filename: "[name].js",
        chunkFilename: "[chunkhash].js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules|vue\/src|vue-router\//,
                loader: 'babel-loader'
            },
            {test: /\.vue$/, loader: 'vue-loader'}
        ]
    },
    plugins: [
        new webpack.DllReferencePlugin({
            context: __dirname,
            manifest: require('./build/vendor/vendor-manifest.json')
        })
    ],
    resolve: {alias: {vue: 'vue/dist/vue.js'}}
};
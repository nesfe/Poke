const gulp = require('gulp'),
    os = require('os'),
    gutil = require('gulp-util'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    gulpOpen = require('gulp-open'),
    cssmin = require('gulp-cssmin'),
    // spriter = require('gulp-css-spriter'),
    base64 = require('gulp-css-base64'),
    webpack = require('webpack'),
    webpackConfig = require('./webpack.config.js'),
    connect = require('gulp-connect'),
    config = require('./config.json');
/**
 * 获取浏览器
 * */
const browser = os.platform() === 'linux' ? 'Google chrome' : (os.platform() === 'darwin' ? 'Google chrome' : (os.platform() === 'win32' ? 'chrome' : 'firefox'));
/**
 * 图片拷贝
 * */
gulp.task('copy:images', function (done) {
    gulp.src([config.app + '/images/**/*']).pipe(gulp.dest(config.build + '/images')).on('end', done);
});
/**
 * 处理 Less -> css
 * */
gulp.task('lessmin', function (done) {
    gulp.src([config.app + '/styles/main.less', config.app + '/styles/*.css'])
        .pipe(less())
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(config.build + '/styles'))
        .on('end', done);
});
/**
 * 拷贝HTML
 * */
gulp.task('copy:html', function (done) {
    gulp.src([config.app + '/*.html'])
        .pipe(gulp.dest(config.build))
        .on('end', done);
});
/**
 * 雪碧图
 * */
// gulp.task('sprite', ['copy:images', 'lessmin'], function (done) {
//     var timestamp = +new Date();
//     gulp.src('dist/css/style.min.css')
//         .pipe(spriter({
//             spriteSheet: 'dist/images/spritesheet' + timestamp + '.png',
//             pathToSpriteSheetFromCSS: '../images/spritesheet' + timestamp + '.png',
//             spritesmithOptions: {
//                 padding: 10
//             }
//         }))
//         .pipe(base64())
//         .pipe(cssmin())
//         .pipe(gulp.dest('dist/css'))
//         .on('end', done);
// });
/**
 * 监听
 * */
gulp.task('watch', function (done) {
    gulp.watch(config.app + '/*.html', ['copy:html', 'html:live']);
    gulp.watch(config.app + '/styles/*', ['lessmin', 'html:live']);
    gulp.watch(config.app + '/images/*', ['copy:images', 'html:live']);
    gulp.watch(config.app + '/scripts/*', ['build-js', 'html:live']);
    gulp.watch(config.app + '/scripts/*/*', ['build-js', 'html:live']);
});
/**
 * 页面刷新
 * */
gulp.task('html:live', function () {
    gulp.src('./*').pipe(connect.reload());
});
/**
 * 创建测试服
 * */
gulp.task('connect', function () {
    connect.server({
        port: config.port,
        root: config.root,
        livereload: config.livereload
    });
});
/**
 * 打开测试服
 * */
gulp.task('open', function (done) {
    gulp.src('')
        .pipe(gulpOpen({
            app: browser,
            uri: config.openUrl
        }))
        .on('end', done);
});
/**
 * 运行webpack
 * */
var myDevConfig = Object.create(webpackConfig);
var devCompiler = webpack(myDevConfig);
gulp.task("build-js", ['copy:html'], function (callback) {
    devCompiler.run(function (err, stats) {
        if (err) throw new gutil.PluginError("webpack:build-js", err);
        gutil.log("[webpack:build-js]", stats.toString({
            colors: true
        }));
        callback();
    });
});
/**
 * 发布
 * */
//gulp.task('default', ['connect', 'fileinclude', 'md5:css', 'md5:js', 'open']);
/**
 * 开发
 * */
// gulp.task('default', ['connect', 'copy:images', 'lessmin', 'build-js', 'watch', 'open']);
gulp.task('default', ['copy:images', 'lessmin', 'build-js', 'watch']);